// @ts-check
// @ts-ignore
import { render } from 'stache';

/** @param {Element | Document} context */
export function renderTemplates(context=document) {
   Array.from(context.querySelectorAll('template[rendered]'))
      .forEach(renderTemplate);
}

/** @param {HTMLTemplateElement | any} $template */
export function renderTemplate($template) {
   if (!($template instanceof HTMLTemplateElement)) return;
   const document = $template.ownerDocument;
   const fragment = $template.content
   const [$metas, data] = parseMeta(fragment);
   const template = extractHtml(fragment);
   const $el = document.createElement($template.getAttribute('as') || 'div'); // @ts-ignore:
   $el.innerHTML = render(template, data); // @ts-ignore
   $metas.reverse().forEach($meta => $el.prepend($meta))
   $template.replaceWith($el)
}

/** @param {DocumentFragment} fragment */
function extractHtml(fragment) {
   return /** @type {String} */ (
      [].map.call(fragment.childNodes, $ => $.outerHTML).join(''))
}

/** @param {DocumentFragment} fragment */
function parseMeta(fragment) {
   const $metas = Array.from(fragment.querySelectorAll('meta'));
   const data = {};
   $metas.forEach($meta => {
      $meta.remove();
      $meta.getAttributeNames().forEach(key => {
         data[key] = $meta.getAttribute(key)
      })
   })
   return [$metas, data]
}