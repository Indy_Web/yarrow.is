// @ts-check

Object.defineProperties(window, {
    startViewTransition: {
        value: startViewTransition,
        writable: false
    }
})

import { renderTemplates } from './directive/template-render.mjs';
document.addEventListener("DOMContentLoaded", (event) => {
    renderTemplates()
    applyTitle(document.querySelector('main') || document.body);
    applyBase()
 }, { once: true });


if ('navigation' in window) // @ts-ignore:
window.navigation.addEventListener('navigate', (event) => {
    if (!(event instanceof Event)) return;

    // @ts-ignore:
    const url = new URL(event.destination.url)
    // @ts-ignore:
    const { navigationType: type } = event //? 'push', 'reload', 'replace', 'traverse'
    //const handler = updateContent;
    // @ts-ignore:
    event.intercept({ handler: () => updateContent(url) })
    console.log(event, `navigation (${type}) to:`, url)
})

const parser = new DOMParser();
async function updateContent(url) {
    console.log('updateContent:', url)
    const response = await fetch(url, {
        headers: {
            "Is-View-Transition": "true"
        }
    });

    const selector = 'main';
    const html = await response.text();
    const $source = processHtml(html, selector)
    const $target = document.body.querySelector(selector);
    if (!$target) return;
    await startViewTransition(() => {
        window.scrollTo(0,0);
        applyTitle($source); //? $target
        applyBase()
        $target.replaceWith($source);
    });
}

/**
 * @param {String} html
 * @param {String} selector */
export function processHtml(html, selector='main') {
   const $html = parser.parseFromString(html, 'text/html');
   const $source = $html.querySelector(selector);
   if (!$source) return new DocumentFragment();
   renderTemplates($source);
   const range = new Range();
   range.selectNode($source);
   return range.extractContents();
}

/** @param {Element | Document | DocumentFragment} $from */
function applyTitle($from) {
    console.log('applyTitle()')
    const $source = /** @type {HTMLMetaElement} */
        ($from.querySelector(`meta[title]`));
    if (!$source) return;
    const value = $source.getAttribute('title') || '';
    if (!value) return;
    const $target = document.head.querySelector('title')
        || document.createElement('title');
    $target.innerText = value;
    console.log('title: ', value)
    if ($target.isConnected) return;
    document.head.prepend($target);
}

function applyBase() {
    const $base = document.head.querySelector('base')
        || document.createElement('base');
    $base.setAttribute('href', location.pathname);
    if ($base.isConnected) return;
    document.head.querySelector('link, script')?.before($base)
        ?? document.head.append($base);
}

/** 
 * @param {Function|any} updateDOM 
 * @returns {Promise<any>} */
function startViewTransition(updateDOM) {
    if (!(updateDOM instanceof Function)) {
        console.warn("Provide a Function that updates the DOM to 'startViewTransition'.")
        return Promise.resolve();
    }
    return ('startViewTransition' in document) // @ts-ignore:
        ? document.startViewTransition(updateDOM).finished
        : Promise.resolve(updateDOM())
}