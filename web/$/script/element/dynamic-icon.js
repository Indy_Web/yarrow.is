//@ ts-check

const icons = '/$/icon'
const size = '1em'
const fill = {
    primary: 'var(--color-text-heading)',
    secondary: 'var(--color-bg-body)'
}
const color = fill.primary

/**
 * @param {string} src
 * @param {{
 *  size: string
 *  fill: {
 *    primary: string,
 *    secondary: string
 *  }
 *  color: string
 *  debug: boolean
 * }} [options]
 */
async function loadIcon(src, options) {
    if (options?.debug) {
        console.log('loading icon:', src)
        console.log('into:', this)
    }
    const $template = document.createElement('template')
    try {
        // ToDo: Sanitize fetched SVG content?
        /*
        const svg = await (await fetch(source)).text()
        if ('setHTML' in $template)
            $template.setHTML(svg)
        else
            $template.innerHTML = svg
        */

        //? const innerSVG = sanitizer.sanitizeFor('svg', await (await fetch(source)).text())
        //? $template.innerHTML = innerSVG //? await (await fetch(source)).text()
        $template.innerHTML = await (await fetch(src)).text()
    }
    catch {
        return;
    }

    this.classList.add('icon')

    if (!this.shadowRoot) this.attachShadow({ mode: 'open' })
    else this.shadowRoot.innerHTML = ''
    this.shadowRoot.appendChild($template.content.cloneNode(true))
    const $svg = this.shadowRoot?.querySelector('svg')
    if (!$svg) return;
    if (options?.size) {
        $svg.setAttribute('width', options.size)
        $svg.setAttribute('height', options.size)
        $svg.style.width = 'inherit';
        $svg.style.height = 'inherit';
    }
    if (options?.fill?.primary)
        $svg.setAttribute('fill', options.fill.primary)
    if (options?.color)
        $svg.style.color = options.color;
    return $svg
}


//* Custom icon needs to observe itself and update when `src` changes.
customElements.define(
    'dynamic-icon',
    class extends HTMLElement {
        /** @type {MutationObserver} */
        #observer;

        constructor() {
            super();
            this.#observer = new MutationObserver((mutations) => {
                for (const mutation of mutations) {
                    console.log('observing <dynamic-icon>')
                    if (mutation.type === 'attributes' &&
                        mutation.attributeName === 'src'
                    ) {
                        console.log('reloading <dynamic-icon>')
                        return (this.getAttribute('src'))
                            ? this.#load()
                            : undefined
                    }
                }
            })
            if (this.getAttribute('src')) this.#load()
        }

        #load() {
            this.#observer.disconnect()

            const src = this.getAttribute('src')
            if (!src) {
                console.warn("The <dynamic-icon> element requires that 'src' is defined.")
                return;
            }
            const customFill = this.getAttribute('fill') || fill.primary
            loadIcon
                .call(this, src, { fill, size })
                .then($svg => {
                    //! $svg?.querySelectorAll('path:not([fill])')
                    //!     ?.forEach($ => $.setAttribute('fill', customFill))
                    $svg?.querySelectorAll('path:not([fill])')
                        ?.forEach($ => $.setAttribute('fill', customFill))
                })
        
            this.#observer.observe(this, {
                attributeFilter: ['src']
            })


            //ToDo: Should inherit color?
        }
    }
)

/* https://icon-sets.iconify.design/fa/github/ */
customElements.define(
    "gitlab-color-icon",
    class extends HTMLElement {
        constructor() {
            super();
            loadIcon
                .call(this, `${icons}/gitlab-color.svg`, { size })
        }
    },
);

/* https://icon-sets.iconify.design/fa/github/ */
customElements.define(
    "pencil-icon",
    class extends HTMLElement {
        constructor() {
            super();
            loadIcon
                .call(this, `${icons}/pencil.svg`, { size })
        }
    },
);
//? <dynamic-icon src="/$/icon/pencil.svg" size>

/* https://icon-sets.iconify.design/carbon/logo-gitlab/ */
customElements.define(
    "gitlab-icon",
    class extends HTMLElement {
        constructor() {
            super();
            loadIcon
                .call(this, `${icons}/gitlab.svg`, { fill, size })
        }
    },
);

/* https://icon-sets.iconify.design/fa/github/ */
customElements.define(
    "github-icon",
    class extends HTMLElement {
        constructor() {
            super();
            loadIcon
                .call(this, `${icons}/github.svg`, { fill, size })
        }
    },
);

/* https://icon-sets.iconify.design/logos/telegram/ */
customElements.define(
    "telegram-color-icon",
    class extends HTMLElement {
        constructor() {
            super();
            loadIcon
                .call(this, `${icons}/telegram-color.svg`, { size })
        }
    },
);

/* https://handshake.org/images/landing/logo-dark.svg */
customElements.define(
    "handshake-icon",
    class extends HTMLElement {
        constructor() {
            super();
            loadIcon
                .call(this, `${icons}/handshake.svg`, { size })
        }
    },
);


/* https://icon-sets.iconify.design/icon-park/telegram/ */
customElements.define(
    "telegram-icon",
    class extends HTMLElement {
        constructor() {
            super();
            loadIcon
                .call(this, `${icons}/telegram.svg`, { fill, size })
        }
    },
);


/* https://icon-sets.iconify.design/fa/twitter/ */
customElements.define(
    "twitter-icon",
    class extends HTMLElement {
        constructor() {
            super();
            loadIcon
                .call(this, `${icons}/twitter.svg`, { fill, size })
        }
    },
);

/* https://icon-sets.iconify.design/simple-icons/unlicense/ */
customElements.define(
    'unlicense-icon',
    class extends HTMLElement {
        constructor() {
            super();
            loadIcon
                .call(this, `${icons}/unlicense.svg`, { size, color })
        }
    }
)

/* https://icon-sets.iconify.design/skill-icons/ipfs-light/ */
customElements.define(
    'ipfs-icon',
    class extends HTMLElement {
        constructor() {
            super();
            loadIcon
                .call(this, `${icons}/ipfs.svg`, { size })
                .then($svg => {
                    //! $svg.querySelector('rect').remove()
                    $svg?.querySelector('rect').remove()
                })
        }
    }
)

/* https://icon-sets.iconify.design/logos/brave/ */
customElements.define(
    'brave-color-icon',
    class extends HTMLElement {
        constructor() {
            super();
            loadIcon
                .call(this, `${icons}/brave-color.svg`, { size })

            // ToDo: allow SVG to inherit 'transform' property from this.style
        }
    }
)

/* https://icon-sets.iconify.design/logos/brave/ */
customElements.define(
    'brave-icon',
    class extends HTMLElement {
        constructor() {
            super();
            loadIcon
                .call(this, `${icons}/brave.svg`, { color, size })
        }
    }
)

/* https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg */
customElements.define(
    'cc-zero-icon',
    class extends HTMLElement {
        constructor() {
            super();
            loadIcon //? https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg
                .call(this, `${icons}/cc-zero.svg`)
                .then($svg => {
                    $svg?.querySelectorAll('path:not([fill])')
                        .forEach($ => $.setAttribute('fill', fill.primary))
                    $svg?.querySelectorAll('[fill="#FFFFFF"]')
                        .forEach($ => $.setAttribute('fill', fill.secondary))
                    $svg?.querySelector('&> g> path:nth-of-type(3)').remove()
                })
        }
    }
)

/* https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/publicdomain.svg */
customElements.define(
    'public-domain-badge',
    class extends HTMLElement {
        constructor() {
            super();
            loadIcon
                .call(this, `${icons}/public-domain-badge.svg`)
                .then($svg => {
                    $svg?.querySelectorAll('path:not([fill])')
                        .forEach($ => $.setAttribute('fill', fill.primary))
                    $svg?.querySelectorAll('[fill="#FFFFFF"]')
                        .forEach($ => $.setAttribute('fill', fill.secondary))
                    //? $svg?.querySelector('&> g> path:nth-of-type(3)').remove()
                })
        }
    }
)


/* https://creativecommons.org/about/downloads/ */
customElements.define(
    'cc-heart-icon',
    class extends HTMLElement {
        constructor() {
            super();
            loadIcon
                .call(this, `${icons}/cc-heart.svg`, { fill, size })
                .then($svg => {
                    //! $svg.querySelector('defs').remove()
                    $svg?.querySelector('defs').remove()
                })
        }
    }
)

/* https://icon-sets.iconify.design/uil/align/ */
customElements.define(
    'align-icon',
    class extends HTMLElement {
        constructor() {
            super();
            loadIcon
                .call(this, `${icons}/align.svg`, { fill, size })
        }
    }
)

/* https://icon-sets.iconify.design/uil/align-left/ */
customElements.define(
    'align-left-icon',
    class extends HTMLElement {
        constructor() {
            super();
            this.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" d="M3 7h18a1 1 0 0 0 0-2H3a1 1 0 0 0 0 2Zm0 4h14a1 1 0 0 0 0-2H3a1 1 0 0 0 0 2Zm18 2H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Zm-4 4H3a1 1 0 0 0 0 2h14a1 1 0 0 0 0-2Z"/></svg>`
            this.querySelector('svg').setAttribute('fill', 'var(--color-text-heading)')
        }
    }
)

/* https://icon-sets.iconify.design/uil/align-center/ */
customElements.define(
    'align-center-icon',
    class extends HTMLElement {
        constructor() {
            super();
            this.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" d="M3 7h18a1 1 0 0 0 0-2H3a1 1 0 0 0 0 2Zm4 2a1 1 0 0 0 0 2h10a1 1 0 0 0 0-2Zm14 4H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Zm-4 4H7a1 1 0 0 0 0 2h10a1 1 0 0 0 0-2Z"/></svg>`
            this.querySelector('svg').setAttribute('fill', 'var(--color-text-heading)')
        }
    }
)

/* https://icon-sets.iconify.design/uil/align-right/ */
customElements.define(
    'align-right-icon',
    class extends HTMLElement {
        constructor() {
            super();
            this.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" d="M3 7h18a1 1 0 0 0 0-2H3a1 1 0 0 0 0 2Zm18 10H7a1 1 0 0 0 0 2h14a1 1 0 0 0 0-2Zm0-8H7a1 1 0 0 0 0 2h14a1 1 0 0 0 0-2Zm0 4H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Z"/></svg>`
            this.querySelector('svg').setAttribute('fill', 'var(--color-text-heading)')
        }
    }
)

/* https://icon-sets.iconify.design/uil/align-justify/ */
customElements.define(
    'align-justify-icon',
    class extends HTMLElement {
        constructor() {
            super();
            this.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" d="M3 7h18a1 1 0 0 0 0-2H3a1 1 0 0 0 0 2Zm18 10H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Zm0-4H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Zm0-4H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Z"/></svg>`
            this.querySelector('svg').setAttribute('fill', 'var(--color-text-heading)')
        }
    }
)

/* https://icon-sets.iconify.design/uil/align-left-justify/ */
customElements.define(
    'align-left-justify-icon',
    class extends HTMLElement {
        constructor() {
            super();
            this.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" d="M3 5h18a1 1 0 0 0 0-2H3a1 1 0 0 0 0 2Zm12 14H3a1 1 0 0 0 0 2h12a1 1 0 0 0 0-2Zm6-8H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Zm0-4H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Zm0 8H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Z"/></svg>`
            this.querySelector('svg').setAttribute('fill', 'var(--color-text-heading)')
        }
    }
)

/* https://icon-sets.iconify.design/uil/align-center-justify/ */
customElements.define(
    'align-center-justify-icon',
    class extends HTMLElement {
        constructor() {
            super();
            this.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" d="M21 15H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2ZM3 5h18a1 1 0 0 0 0-2H3a1 1 0 0 0 0 2Zm14 14H7a1 1 0 0 0 0 2h10a1 1 0 0 0 0-2Zm4-12H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Zm0 4H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Z"/></svg>`
            this.querySelector('svg').setAttribute('fill', 'var(--color-text-heading)')
        }
    }
)

/* https://icon-sets.iconify.design/uil/align-right-justify/ */
customElements.define(
    'align-right-justify-icon',
    class extends HTMLElement {
        constructor() {
            super();
            this.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" d="M3 5h18a1 1 0 0 0 0-2H3a1 1 0 0 0 0 2Zm18 14H11a1 1 0 0 0 0 2h10a1 1 0 0 0 0-2Zm0-8H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Zm0 4H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Zm0-8H3a1 1 0 0 0 0 2h18a1 1 0 0 0 0-2Z"/></svg>`
            this.querySelector('svg').setAttribute('fill', 'var(--color-text-heading)')
        }
    }
)

/* https://icon-sets.iconify.design/uil/arrow/ */
customElements.define(
    'element-icon',
    class extends HTMLElement {
        constructor() {
            super();
            loadIcon
                .call(this, `${icons}/arrow.svg`, { fill, size })
        }
    }
)

/* https://icon-sets.iconify.design/fluent-mdl2/edit-style/ */
customElements.define(
    'style-icon',
    class extends HTMLElement {
        constructor() {
            super();
            loadIcon
                .call(this, `${icons}/edit-style-fluent.svg`, { fill, size })
        }
    }
)

/* https://icon-sets.iconify.design/mdi/style-outline/ */
customElements.define(
    'style-outline-icon',
    class extends HTMLElement {
        constructor() {
            super();
            loadIcon
                .call(this, `${icons}/style-outline.svg`, { fill, size })
        }
    }
)

customElements.define(
    'icon-icon',
    class extends HTMLElement {
        constructor() {
            super();
            loadIcon
                .call(this, `${icons}/dynamic-icon.svg`)
                .then($svg => {
                    $svg.querySelectorAll('path:not([fill])')
                        .forEach($ => $.setAttribute('fill', fill.primary))
                })
        }
    }
)
