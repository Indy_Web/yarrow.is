// @ts-check

export const ϕ = 1.618;
export const φ = 1 / ϕ;

/**
 * @param {string} style
 * @param {string} element */
export function appendStyle(style, element) {
   const $style = document.createElement('style');
   $style.setAttribute('element', element);
   $style.innerHTML = style;
   document.head.appendChild($style);
}

/** @param {string} html */
export function renderTemplate(html) {
   return document.createRange()
       .createContextualFragment(html)
}

/** @param {Element} $ */
export function cleanElement($) {
   if ($.hasAttribute('class') && !$.classList.length)
       $.removeAttribute('class')
   if ($.hasAttribute('style') && !$.getAttribute('style'))
       $.removeAttribute('style')
}

export function round(number, to=100) {
   return Math.round(number * to) / to
}