// @ts-check

customElements.define('underlined-on-hover',
class extends HTMLAnchorElement {
   constructor() {
      super();
      window.addEventListener('popstate', (event) => {
         if (this.isCurrentRoute) {
            this.setAttribute('loaded', "")
         } else {
            this.removeAttribute('loaded');
         }
      });
   }

   get isCurrentRoute() {
      return Boolean(this.getAttribute('href') === window.location.pathname)
   }
}, { extends: 'a' });

const style = /* css */`
a[is="underlined-on-hover"] {
   display: inline-block;
   position: relative;
   cursor: pointer;
   
   &[loaded] {
      pointer-events: none;
   }
   
   &:is([loaded], :hover) {
      font-weight: 600;
   }
   
   text-decoration: none;
   color: var(--link-color);
}
a[is="underlined-on-hover"]::after {
   content: '';
   position: absolute;
   width: 100%;
   transform: scaleX(0);
   height: 2px;
   bottom: 0;
   left: 0;
   background-color: var(--color-text-bold);
   transform-origin: bottom right;
   transition: all 0.25s ease-out;
}
a[is="underlined-on-hover"]:hover::after {
   //background-color: rgb(73 18 46);
   transform: scaleX(1);
   transform-origin: bottom left;
}`

import { appendStyle } from './_shared.mjs';
appendStyle(style, 'a[is="underlined-on-hover"]');