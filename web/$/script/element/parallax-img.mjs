import { ϕ, round } from './_shared.mjs';
const heightDefault = `${round(window.innerHeight / ϕ)}px`;

const style = /* css */`
@keyframes parallax-img {
   from {
       transform: translateY(calc(-1 * var(--translate)));
   }
   to {
       transform: translateY(var(--translate));
   }
}

div {
   width: var(--width, 100%);
   height: var(--height, ${heightDefault});
   overflow: hidden;
   position: relative;
   & img {
      position: absolute;
      width: inherit;
      left: 0;
      top: var(--top, 0);
      animation-name: parallax-img;
      animation-timing-function: var(--easing, var(--easing, cubic-bezier(0, 0.125, 1, 0.825)));
      animation-timeline: scroll(root);
   }
}`;

customElements.define('parallax-img',
class ParallaxImage extends HTMLElement {
   width = 'inherit';
   height = heightDefault

   $div = document.createElement('div');
   $style = document.createElement('style');
   $img = new Image();
   translate = 0;
   top = 0;
   scroll = {
      start: 0,
      end: 0
   }

   constructor() {
      super();
      this.$img.loading = 'eager';
      this.$img.src = this.getAttribute('src') || '';
      this.$img.addEventListener('load', () => {
         this.connectedCallback()
      }, { once: true })

      this.height = this.getAttribute('height')
         || this.height;

      this.$div.setAttribute('style', this.$divStyle);
      this.$div.append(this.$img)
      this.$div.append(this.$style)
      this.$style.innerHTML = style;
      this.attachShadow({ mode: 'open' });
      this.shadowRoot?.appendChild(this.$div);
   }

   connectedCallback() {
      this.calculate();
      this.render();
   }

   calculate() {
      const { offsetTop, offsetHeight } = this.$div
      const S = [
         () => offsetTop - window.innerHeight,
         () => offsetTop - window.innerHeight + offsetHeight,
         () => offsetTop,
         () => offsetTop + offsetHeight
      ]
      this.scroll.start = S[0]()
      this.scroll.end = S[3]()
      const ΔS = (start, end) => S[end]() - S[start]()

      const ΔT1_2 = ((this.$img.height - offsetHeight) / this.$img.height * 100);
      const Δ = ΔT1_2 / ΔS(1,2);
      const ΔT = Δ * ΔS(0,3);

      this.top = round(-(this.$img.height - offsetHeight) / offsetHeight * 100 / 2)
      this.translate = round(ΔT / 2)
   }

   render() {
      this.$img.setAttribute('style', this.$imgStyle);
   }

   get $divStyle() {
      const styles = [];
      if (this.hasAttribute('easing'))
         styles.push(`--easing: ${this.getAttribute('easing')};`)
      if (this.height)
         styles.push(`--height: ${this.height};`)
      return styles.join(' ');
   }

   get $imgStyle() {
      const styles = [];
      styles.push(`animation-range-start: ${this.scroll.start}px;`),
      styles.push(`animation-range-end: ${this.scroll.end}px;`),
      styles.push(`--translate: ${this.translate}%;`),
      styles.push(`--top: ${this.top}%;`)
      return styles.join(' ')
   }
});

window.addEventListener('load', () => {
   document.querySelectorAll('parallax-img')
      .forEach($ => $.connectedCallback());
});