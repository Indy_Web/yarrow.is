// @ts-check

const parser = new DOMParser();

const style = /* css */`
* {
   color: var(--color-text-muted);
   font-weight: 300;
   &:hover {
      font-weight: 400;
   }
}
a:not(:hover) {
   text-decoration: none;
}
span {
   cursor: var(--cursor-clip);
   &:hover {
      text-decoration: underline;
   }
}`

customElements.define('route-navigation', class extends HTMLElement {
   route = location.pathname
   constructor() {
      super();
      this.attachShadow({ mode: 'open' });
   }

   connectedCallback() {
      if (location.pathname === '/') return;
      const $ = parse(this.template())
      this.shadowRoot?.replaceChildren($);
      this.shadowRoot?.querySelector('span')
         ?.addEventListener('click', this.copyLink);
   }

   template() {
      const slugs = location.pathname.split('/').slice(1,)
      const href = (/** @type {Number} */ s) => ['', ...slugs.slice(0,s+1)].join('/')
   
      let template = '';
      slugs.slice(0,-1).forEach((slug, s) => {
         template += `/<a href="${href(s)}">${slug}</a>`
      })
      template += `/<span href="${location.origin+href(slugs.length-1)}" title="Copy link.">${slugs.slice(-1)}</span>`;
      template += `<style>${style}</style>`
      return template;
   }

   /** @param {MouseEvent} event */
   copyLink(event) {
      const href = (event.target instanceof HTMLElement)
         ? event.target.getAttribute('href') || ''
         : location.origin
      navigator.clipboard.writeText(href);
   }
}, {
   extends: 'nav'
});

/** @param {String} html */
function parse(html) {
   const range = new Range();
   range.selectNodeContents(parser.parseFromString(html, 'text/html').body);
   return range.extractContents();
}