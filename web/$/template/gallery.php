<?php require './journal.php';
$GLOBALS['hero'] = [
   'height' => "350px"
];

function render($article, $slug) {
   $height = $GLOBALS['hero']['height'];
   $title = $article['title'];
   $href = joinPaths($_SERVER['REQUEST_URI'], $slug);
   $src = joinPaths($_SERVER['REQUEST_URI'], $slug, $article['src']);

return <<<ARTICLE
<a title="$title" href="$href">
   <parallax-img
      src="$src"
      height="$height">
   </parallax-img>
</div>
ARTICLE;
};?>

<section class="gallery">

<!-- testing: 
   <a href="/philosopher/making-space-for-a-politics-of-the-people" title="Making Space for a Politics of the People">
      <parallax-img
         src="/philosopher/making-space-for-a-politics-of-the-people/photo-by-Jaredd-Craig.avif"
         height="350px">
      </parallax-img>
   </a>
 testing: -->

<?php 
foreach ($journal['articles'] as $slug => $article) {
   echo render($article, $slug);
};?>

</section>
<style>
.gallery {
   &> * {
      margin-top: 2rem;
      display: block;
   }

   & parallax-img:hover {
      view-transition-name: hero;
   }
}
</style>