<section id=utilities>
   <?php includeRaw('/$/template/light-switch.html') ?>
   <a href="/calling">
      <dynamic-icon src="/$/icon/phone-call.svg" style="height: 1.5em"></dynamic-icon>
   </a>

<style>
#utilities {
   position: absolute;
   top: 1.25em;
   right: -2.5em;
   transition: right 0.2s ease;
   &[visible] {
      right: 0;
   }

   display: grid;
   gap: 0.5em;

   background-color: var(--color-bg-accent);
   padding: 0.5em;
   padding-right: 0.75em;
   border-radius: 1em 0 0 1em;
}
</style>
<script>
(() => {
   const $header = document.querySelector('header');
   $header.addEventListener('intersecting', ({ detail }) => {
      const { isIntersecting } = detail;
		console.log('header is intersecting:', isIntersecting)
      if (isIntersecting)
         document.getElementById('utilities').setAttribute('visible', '');
      else
         document.getElementById('utilities').removeAttribute('visible');
   })
})()
</script>
</section>