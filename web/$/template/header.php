<header>
	<nav>
		<a is="underlined-on-hover" href="/">Yarrow.is</a>:
		<a is="underlined-on-hover" href="/philosopher"><i>philosopher</i></a>,
		<a is="underlined-on-hover" href="/developer"><i>developer</i></a>,
		<a is="underlined-on-hover" href="/activist"><i>activist</i></a>,
		<a is="underlined-on-hover" href="https://ouroborealis.surge.sh/" target="_blank"><i>artist</i></a>,
		<span class="hover-bold">creator</span>.
	</nav>
	
<style>
body> header {
	display: flex;
	align-items: center;
	justify-content: space-between;

	font-family: var(--font-heading);
	color: var(--color-text-heading);
	user-select: none;
	contain: paint;
}
</style>

<script>
(new IntersectionObserver(entries => {
	entries.forEach(entry => {
		entry.target.dispatchEvent(new CustomEvent('intersecting', { detail: {
			isIntersecting: Boolean(entry.isIntersecting)
		} }));
	});
})).observe(document.querySelector('header'));
</script>
</header>