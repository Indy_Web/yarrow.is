<footer>
	<a title="This site is a protected cultural artifact." target="_blank" href="https://www.roerich.org/roerich-pact.php"><img src="/$/icon/banner-of-peace.svg" alt="The Banner of Peace." style="width: 3em;"></a><br>
	<p>© yarrow.is (<?php echo date("Y"); ?>)</p>
	<p>All Rights Reserved by<br><author><b>Justin Lane: <i>Yarbrough</i></b></author></p>
	<a title="Civilian on the land of the u.S. of A." href="https://thelawdictionary.org/civilian/" target="_blank">
		<img id="civil-flag" alt="The civilian flag for the u.S.A." src="/$/icon/civil-flag.svg"></a>

<style>
body> footer {
	position: relative;
	user-select: none;
	margin-top: 2rem;
	contain: paint;

	text-align: center;
	line-height: 1.5em;
	font-size: 0.875rem;
	color: var(--color-text-heading);
	/* font-family: var(--font-heading); */


	& author {
		font-size: 1.1em;
	}

	& #civil-flag {
		position: absolute;
		bottom: 3em; left: 0;
		height: 3em;
	}
}
</style>
</footer>