<?php require_once realpath($_SERVER['DOCUMENT_ROOT'] . "/utils.php"); ?>
<!DOCTYPE html>
<html lang="en">
<?php includeRaw('/$/template/head.html'); ?>
<body theme="dark">
  <?php include '$/template/header.php'; ?>
  <?php include '$/template/utils.php'; ?>
  <main revealed="<?php echo isViewTransition() ? "true" : "false" ?>">
    <nav is="route-navigation"></nav>
    <?php 
    if (file_exists('./main.php')) {
      include './main.php';
    }
    elseif (file_exists('./main.html')) {
      include './main.html';
    } ?>
  </main>
  <?php include '$/template/footer.php'; ?>
</body>
</html>
<?php includeRaw('/ascii.html'); ?>