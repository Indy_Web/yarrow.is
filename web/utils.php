<?php
function getSlug() {
	return end(explode('/', dirname($_SERVER['PHP_SELF'])));
}

function getRealPath(string $path) {
	return $_SERVER['DOCUMENT_ROOT'] . $path;
}

function includeRaw(string $path) {
	readfile(getRealPath($path));
}

function isViewTransition() {
	return array_key_exists('Is-View-Transition', getallheaders());
}

/**
 * Join string into a single URL string:
 * @param string $parts,... The parts of the URL to join.
 * @return string The URL string. */
function joinPaths(...$parts) {
	if (sizeof($parts) === 0) return '';
	$prefix = ($parts[0] === DIRECTORY_SEPARATOR) ? DIRECTORY_SEPARATOR : '';
	$processed = array_filter(array_map(function ($part) {
		 return rtrim($part, DIRECTORY_SEPARATOR);
	}, $parts), function ($part) {
		 return !empty($part);
	});
	return $prefix . implode(DIRECTORY_SEPARATOR, $processed);
}
?>