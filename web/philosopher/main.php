<article>
   <meta title="Yarrow.is <philosopher>">

   <h2>I AM a <em>philosopher</em>.</h2>

   <parallax-img src="/philosopher/khôra.png" height="500px"></parallax-img>
   <h2><a href="https://medium.com/the-khôra-project" target="_blank">The Khôra Project</a></h2>

   <p>The Khora Project is a vision for a better future. The human race has a unique opportunity at this moment in history. The technologies of consciousness that have emerged from the coordinated work of open-source software developers provide us the tools for supporting democratic networks that radically empower the wisdom and intelligence of all people, valuing a person’s voice according to their integrity and the merit of their presentation as perceived by their peers.</p>

   <p>We intend to design a democratic process that engages everyone, that values critical thinking and self-expression, that makes way for all perspectives wherever they stand, because we believe that being honored and engaged will transform minds and hearts. We intend to encourage a culture that promotes self-questioning and mutual learning, that honors the living intelligence of all human beings, that challenges everyone to think deeply, to express themselves clearly, carefully, vividly. We wish to empower the artist, all truth-telling artforms, reintegrating the arts with science and politics.</p>

   <p>All perspectives count. Gone are the days of epistemological oppression when individual expression could be suppressed by the centralization of media power. Social media has reversed the implicit message of broadcast media, affirming that “your voice matters,” but the work is not over. We are only beginning to re-vision the relationships of the individual to the community and to the collective. We envision that these nested spheres of influence will make up the democracy to come, the integral holarchy.</p>

   <p>We need you to carry this mission onward by awakening your heart, expressing your intelligence, and pronouncing your purist intentions, as we are the co-creators of our world.</p>

   <span>Empower wisdom.</span>
   <span>The Khora Project.</span>

   <?php require getRealPath('/$/template/gallery.php'); ?>
</article>