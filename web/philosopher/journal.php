<?php
$journal = [
   'author' => "Yarrow",
   'articles' => [
      'making-space-for-a-politics-of-the-people' => array(
         'title' => "Making Space for a Politics of the People",
         'date' => "April 9, 2020",
         'src' => "photo-by-Jaredd-Craig.avif"
      ),
      'bioregional-holarchy' => [
         'title' => "Bioregional Holarchy",
         'date' => "July 1, 2020",
         'src' => "photo-by-Jesse-Gardner.webp"
      ],
      'dendritic-dissensus' => [
         'title' => "Dendritic Dissensus",
         'date' => "March 28, 2020",
         'src' => "photo-by-Rowan-Heuvel.jpeg"
      ],
      'prayer-to-evoke-creative-spirit' => [
         'title' => "A Prayer to Evoke the Creative Spirit",
         'date' => "March 29, 2020",
         'src' => "photo-by-Johannes-Plenio.avif"
      ],
      'manifesto-for-collective-intelligence' => [
         'title' => "Manifesto for Collective Intelligence",
         'date' => "July 13, 2018",
         'src' => "photo-by-Jeremy-Bishop.avif"
      ],
      'web3-future-of-democracy' => [
         'title' => "Web3 & the Future of Democracy",
         'date' => "July 27, 2020",
         'src' => "photo-by-Randy-Colas.webp"
      ]
   ]
];
?>