<template rendered as="article">
   <meta
      author="yarrow"
      created="July 1, 2020"
      title="Bioregional Holarchy"
      hero="/philosopher/bioregional-holarchy/photo-by-Jesse-Gardner.webp"
   >
   <header>
      <h2>{{title}}</h2>
      <h5>Published on {{created}} by {{author}}.</h5>
   </header>

   <p>We envision democratic practices that evoke and empower the <em>living intelligence</em> of a place and its people. This bioregional intelligence is communicated by the human community through the social processes of creative expression, conversation, critical learning, and co-authorship. That which we creatively express and record becomes the representative material of our collective story. Each person is to be honored in their process of learning, invited to express themselves openly, and valued for their honesty, perceptivity, and coherence within the context of conversation.</p>

   <parallax-img
      src="{{hero}}"
      class="hero"
      height="350px">
   </parallax-img>

   <p>The grassroots democracy that we envision encourages everyone to formally present one’s beliefs and grievances in the open in writing, in audio or film recording, or any other medium. The invitation is to express one’s truth (anonymously if one wishes) in any way one knows how. A person’s sense of truth is organic — growing, branching, exploring, budding, flowering, fruiting, and sometimes decaying. This is why we intend to invite all people into an organic learning process that provides a sustenance of intellectual, emotional, and spiritual nutrition. We advocate for democracy that encourages everyone to give attention to carefully crafting and refining one’s beliefs by way of the yin-yang process of empathic consideration and self-expression. This is the <em>social democratic</em> process of speaking and writing openly to engage the bioregional intelligence of a community.</p>

   <p>Truth comes in many mediums, and we encourage everyone to open and explore their conscience and to participate in <em>communal discussion</em>, listening deeply, considering carefully, and presenting oneself in the open clearly and compassionately. This practice is intended to engage, heal, and evolve the collective mind.</p>

   <p>We envision the establishment of <em>sacred democratic space</em> where one’s expression is valued for its coherence, clarity, and conviction, according to the perception of one’s peers within a multi-dimensional nest of thought communities.</p>

   <p>Human beings want to express their thoughts and feelings in meaningful ways, and we want for wisdom to resound in political life. We are initiating the collective exploration of all belief, housing a democratic engine for critical thinking, self-expression, and coherent co-authorship. We are initiating a revolution of mind, of <em>holographic self-creativity</em> by integrating the possibility of creating the world with our thoughts into a communal practice of truth-telling.</p>

   <hr/>
   
   <h3>Proposal: Bioregional Democracy in Nevada County</h3>

   <p>Documentary film is a powerfully reflective medium that opens pathways for empathy and understanding across cultural divides. Anyone who has felt the significance of an important documentary knows how it catalyzes the ability to see a new perspective, influencing one forever, expanding one’s understanding and sense of context, making way for a more nuanced appreciation of a subject.</p>

   <parallax-img src="/philosopher/bioregional-holarchy/photo-by-Steven-Van.jpg" height="300px"></parallax-img>

   <p>Documentary film is a potent <em>truth-telling art</em> and is paramount to the evolution of democratic practices. The arts have been mistakenly expelled from the realm of politics since Plato, and yet even he wrote in the myth of Khora of a kind of space that opens to the full spectrum of human expression.</p>

   <p>Democracy itself must open to accommodate the full expression of human beings, all of our meaning. We must actualize Mary Parker Follet’s vision (1918) to establish a “creative democracy” powered from the grassroots up, organized into regional coalitions, town-halls that house the social processes of their local, political life.</p>

   <p>To empower grassroots coalitions, we must overcome the distance that separates us, support our memory as a society, and facilitate a process of generative conversation among very large numbers of people. This final point is essential to empowering wisdom in politics and overcoming the acute limitations of bureaucratic power structures. I would like to introduce a vision for facilitating this emergence of communal expression, but first let me highlight the significance of documentary and other film medias.</p>

   <p>Documentary film is essential for bringing distant perspectives into mutual awareness — let’s expand this concept of documentary to also include forms of auto-documentary like recorded videos and long-form conversations that are really forms of auto-documentary, serving the same purpose of presenting perspective. The art of documentary engages the viewer in another life or another world and provides an experience that activates empathy and learning to develop a more complex appreciation and nuanced opinion. This record in film supports community in recalling and referencing their shared memories of an event or circumstance so to engage those ideas in political deliberation. We must provide public infrastructure that archives and addresses (making available for reference) documentary content in a way that is protected against censorship, making it permanently available within the democratic process.</p>

   <p>In order to evolve our society from hierarchical government (and electoral politics) to holarchical government (and self-representation), we will demonstrate in Nevada County how to synergize the creativity of a community by inviting coherent sharing and mutual learning, how to channel that energy into grassroots initiatives, and how to produce co-authored, open-source legislation as a community.</p>

   <p>This is our best chance for initiating a peaceful democratic revolution that empowers the grassroots to engage our wisdom, and our wealth living intelligence. What will we call this bioregional force of mind? The Bioregional Intelligence Organism (BIO)?</p>

   <p>This massively multi-cellular democratic organism can only thrive by honoring the minds of all people — all of our writing and our art has the potential of conveying very significant meaning and power. We must provide ourselves the context in which to embed our creative works so to reveal the emmergent story, to engage in politics in ever more focused, coherent, and productive ways. We are initiating the collective weaving of contextual tapestries from our ideas and expressive works, which make the colors and the texture and the patterns of the whole.</p>

   <p>The tools are available today to scaffold this contextual mediaspace, accommodating the full spectrum of human expression and providing a structural language for charting the relationships we see between media. These tools are called IPFS (the Inter-Planetary File System) and an open-source, distributed graph database called D-graph.</p>

   <p>The corporate media giants have been using graph databases for several years to make sense of the complex relationships between people and events and concepts, etc, to make more money off of its ‘users’. We have yet to wield this technology as an epistemological tool, as a technology of consciousness to support our learning and collaboration and collective integration.</p>

   <p>The tools are freely available and there is no reason not to put them to use to serve our local community by contextualizing everyone’s feedback so to set up a critical process of reflection and revision and co-authorship. This is happening.</p>

   <p>It is important to recognize that whatever the form of media power is at a particular historical moment, that media is the herald of democracy, incredibly powerful in shaping the collective mind. There was the scroll, then the book, then the newspaper, the radio, television, and now there is distributed “social media”. Now is the time to establish an integral mediaspace that belongs to the people, that is designed to engage everyone from where they stand.</p>

   <p>The only “wicked problem” (that has been at the root of all others) has been our failure to healthily process our beliefs as a collective and as communities so to engage every mind in the generation of an omni-subjective autobiography of life on Earth that stands up and represents itself.</p>

   <p>The deceitful myth of salvation by the Free Market or by Artificial Intelligence is a religious denial of human responsibility, creativity, and spirit.</p>

   <p>Now is the time to evoke and share the vision for <em>collective intelligence</em> that will engage our wealth of human wisdom, integrating the intelligence that is distributed throughout the human community by invitation to all to contribute their voice. To engage this sacred wisdom, we self-authorize the creation of an holarchical form of self-government, a form through which Nature itself may speak its truth. So be it.</p>
   
   <p><em>There is philosophy and game theory that has gone into beginning to design this integral mediaspace. I have a conception of the outlines that I would like to present at a later date. I see this design process as something that should be guided by a group thinking process, because it will determine the rules for organizing democratic discourse, so I would like to open this up for consideration and council. I will just say in a few words that all design choices must enable a high signal-to-noise ratio that draws together resonant voices while making way for all self-differentiation of opinion. The value of a voice or of a speaker can only be gauged by other people in a democratic way. I would love to explore this later with those willing to deeply consider the design needs of such a discursive space.</em></p>

</template>